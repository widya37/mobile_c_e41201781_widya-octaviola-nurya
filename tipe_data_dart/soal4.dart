import 'dart:io';

void main() {
  stdout.write("Nilai a: ");
  double a = double.parse(stdin.readLineSync()!);
  stdout.write("Nilai b: ");
  double b = double.parse(stdin.readLineSync()!);

  double hasil;

  // operator perkalian
  hasil = a * b;
  print("Perkalian : $hasil");

  // operator pembagian
  hasil = a / b;
  print("Pembagian : $hasil");

  // operator penambahan
  hasil = a + b;
  print("Penambahan : = $hasil");

  // operator pengurangan
  hasil = a - b;
  print("Pengurangan : = $hasil");
}
